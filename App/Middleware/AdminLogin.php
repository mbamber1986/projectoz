<?php

namespace App\Middleware;

use MiladRahimi\PhpRouter\Middleware;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Closure;

class AdminLogin implements Middleware
{
public function handle(ServerRequestInterface $request, Closure $next)
{
    if(isset($_SESSION['id']))
    {
        return $next($request);
    }
    else
    {
        header("location:/login");
    }



}
}