<?php


namespace App\Http\Errors;

use App\Http\Controllers\BladeController;
use Jenssegers\Blade\Blade;


class Errorlog
{
    public  function Nofolder()
    {
        echo "<h1>An Error Occurred</h1><br/>";
        echo "Sorry it seems you do not have Access to this Directory : this could be due to the limited access or the it has been moved or deleted<br/>";
        echo "if you believe this to be a mistake please contact <a href='mailto:".getenv("ADMIN_EMAIL")."'>Site Admin</a>";
    }

}