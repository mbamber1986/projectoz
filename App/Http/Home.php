<?php
namespace App\Http;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Blade\Blade;
use App\Http\Controllers\Auth;
use App\Http\Users;
use App\Http\Controllers\BladeController;

class Home extends Model
{

    public function index()
    {
        $blade = BladeController::Loadtemplate();
         echo $blade->render("pages.home.index");
}

}