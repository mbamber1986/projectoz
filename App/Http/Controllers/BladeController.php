<?php


namespace App\Http\Controllers;


use Jenssegers\Blade\Blade;
use App\Http\Errors;

class BladeController
{


    public static function AuthViews()
    {
        if(is_dir(__DIR__ . "/../../../App/Auth/Views"))
        {
            $blade = new Blade(__DIR__ . "/../../../App/Auth/Views", __DIR__ . "/../../../Storage/Cache");
            return $blade;
        }
        else
        {
            $errors = new Errors\Errorlog();
            $errors->Nofolder();
            exit();
        }

    }

    public static function Loadtemplate()
    {
        if(is_dir(__DIR__ . "/../../../Storage/views"))
        {
            $blade = new Blade(__DIR__ . "/../../../Storage/views", __DIR__ . "/../../../Storage/Cache");
            return $blade;
        }
        else
        {
            $errors = new Errors\Errorlog();
            $errors->Nofolder();
            exit();
        }

    }


}