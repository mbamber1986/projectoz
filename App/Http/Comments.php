<?php


namespace App\Http;


use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

    public function store()
    {
        $comments = new Comments();
        $comments->user_id = $_POST['user_id'];
        $comments->comments = $_POST['comment'];
        $comments->timestamps;
        $comments->save();
        header("location:/users");
    }

}