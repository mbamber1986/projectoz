<?php


namespace App\Http;


class Requests
{

    public function get($value)
    {
        return $_GET[$value];
    }

    public function post($value)
    {
        return $_POST[$value];
    }

    #
    public function cookie($value)
    {
        return $_COOKIE[$value];
    }

    public function session($value)
    {
        return $_SESSION[$value];
    }

    public function server($value)
    {
        return $_SERVER[$value];
    }

}