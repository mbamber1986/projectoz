<?php


namespace App\Http;

use App\Database\SchemaBuilder;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Blade\Blade;
use App\Http\Controllers\Auth;
use App\Http\Users;
use App\Http\Controllers\BladeController;
use App\Http\Controllers\Redirect;


class Users extends Model


{
    public function index()
    {

     $blade = BladeController::Loadtemplate();
     $users = Users::all();
     $redirect = new Redirect();
    echo $blade->render("pages.users.index",["users"=>$users,"redirect"=>$redirect]);
    }

    public function schema()
    {
        Capsule::schema()->create("users", function ($table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('comments');
            $table->timestamps();
            header("location:/users/success");
        });
    }


    public function byid($username)
    {
        $user = Users::where("username",$username)->first();

        $blade = BladeController::Loadtemplate();
        $comments = \App\Http\Controllers\Users::find($user->id)->comments;
        echo $blade->render("pages/users/profile",["user"=>$user,"comments"=>$comments]);
    }


public function run()
{
    new SchemaBuilder();
}




}
