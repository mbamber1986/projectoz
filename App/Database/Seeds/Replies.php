<?php

namespace App\Database\Seeds;
Use Illuminate\Database\Capsule\Manager as Capsule;


class Replies
{


    public  static function up()
    {
        Capsule::schema()->create('replies', function ($table) {
            $table->increments('id');
            $table->string("commend_id")->unique();
            $table->string("Reply");
            $table->string("User")->unique();
            $table->timestamps();
        });

    }

    static function down()
    {
        Capsule::schema()->dropIfExists("replies");
    }

}