<?php

namespace App\Database\Seeds;
Use Illuminate\Database\Capsule\Manager as Capsule;


class Users
{

    public static function up()
    {
        
        Capsule::schema()->create('users', function ($table) {
            $table->increments('id');
            $table->string("username")->unique();
            $table->string("password");
            $table->string("email")->unique();
            $table->timestamps();
//                Install A new user

        });

        // $users = new \App\Http\Users();
        // $users->username = "Admin";
        // $users->password = password_hash("Admin",PASSWORD_DEFAULT);
        // $users->email = "Admin@website.com";
        // $users->timestamps;
        // $users->save();

    }

    static function down()
    {
        Capsule::schema()->dropIfExists("users");

    }

}