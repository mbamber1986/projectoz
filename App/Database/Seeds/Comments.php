<?php

namespace App\Database\Seeds;
use App\Http\Users;
Use Illuminate\Database\Capsule\Manager as Capsule;


class Comments
{



    public  static function up()
    {
        Capsule::schema()->create('comments', function ($table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('comments');
            $table->timestamps();
        });

        // $users = Users::where('username','Admin')->first();
        // $comments = new \App\Http\Comments();
        // $comments->user_id = $users->id;
        // $comments->comments = "A new Comment";
        // $comments->timestamps;
        // $comments->save();
    }

    static function down()
    {
        Capsule::schema()->dropIfExists("comments");
    }

}