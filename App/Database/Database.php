<?php


namespace App\Database;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;


class Database
{

//    public function __construct()
//    {
//        $this->dsn = getenv("DSN");
//        $this->host = getenv("DBHOST");
//        $this->database = getenv("DBNAME");
//        $this->username = getenv("DBUSER");
//        $this->password = getenv("DBPWD");
//        $this->charset ="utf8";
//        $this->collation = 'utf8_unicode_ci';
//        $this->prefix = "";
//
//    }

    public function LoadDatabase()
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            "driver"    => "mysql",
            'host'      => "localhost",
            'database'  => "martinsblog",
            'username'  => "homestead",
            'password'  => "secret",
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',

//            "driver"    => "{$this->dsn}",
//            'host'      => $this->host,
//            'database'  => $this->database,
//            'username'  => $this->username,
//            'password'  => $this->password,
//            'charset'   => $this->charset,
//            'collation' => $this->collation,
//            'prefix'    => $this->prefix,
        ]);

        $capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
        $capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $capsule->bootEloquent();
    }

}


