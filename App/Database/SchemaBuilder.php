<?php


namespace App\Database;

use App\Database\Seeds;

class SchemaBuilder
{

    public function __construct()
    {
        $this->load();
    }

  
    static function string_pop($string, $delimiter){
      $a = explode($delimiter, $string);
      array_pop($a);
      return implode($delimiter, $a);
  }

  public function load()
{
  
      $dir = __DIR__ . "/Seeds";
      $variables = scandir($dir);
      $files = array_diff($variables, array('.', '..'));
      foreach($files as $file)
      {

      $name = self::string_pop($file,".");

      call_user_func(__NAMESPACE__."\Seeds\\$name::down");
      call_user_func(__NAMESPACE__."\Seeds\\$name::up");

      }

}


}