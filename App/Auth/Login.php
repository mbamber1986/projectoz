<?php


namespace App\Auth;


use App\Http\Controllers\Auth;
use App\Http\Controllers\BladeController;
use App\Http\Users;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Sessions;

class Login extends Model
{

    public function index()
    {
        $blade = BladeController::AuthViews();
        echo $blade->render("login");
    }

    public function store()
    {
        $user = Users::where("username",$_POST['username'])->get()->first();
        $hash = $user->password;
        if(password_verify($_POST['password'],$hash))
        {
            $_SESSION['id'] = $user->id;
            header("location:/auth/login");
        }
        else
        {
            echo "not logged in";
        }
    }

}