<?php

use MiladRahimi\PhpRouter\Router;

$attributes = [
    'prefix'        => '/auth',
    'namespace'     => 'App\Auth',
];

$router->group($attributes, function (Router $router) {

    $router->get("/register","Register@index");
    $router->post("/register","Register@store");
    $router->get("/login","Login@index");
    $router->post("/login","Login@store");
});