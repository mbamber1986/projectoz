<?php


namespace App\Auth;


use Illuminate\Database\Eloquent\Model;
use Jenssegers\Blade\Blade;
use App\Http\Controllers\BladeController;
use App\Http\Users;
use App\Http\Controllers\Auth;

class Register extends Model
{

    public function index()
    {
        unset($_SESSION['id']);
        $blade = BladeController:: AuthViews();
        echo $blade->render("register");
    }

    public function store()
    {
        $users = new Users();
        $users->username = $_POST['username'];
        $users->password = password_hash($_POST['password'],PASSWORD_DEFAULT);
        $users->email = $_POST['email'];
        $users->timestamps;
        $users->save();
        header("location:/users");
    }

}