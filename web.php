<?php
use MiladRahimi\PhpRouter\Router;
use Zend\Diactoros\Response\HtmlResponse;
use MiladRahimi\PhpRouter\Middleware;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

//Set up Router instantiation
$router = new Router('', 'App\Http');
$middleware = App\Middleware\AdminLogin::class;

//Set Routes
$router->any("/","Home@index")->name("homepage");

//$router->get("/users","Users@index");
//$router->post("/users","Users@index");
//$router->get("/users/sql","Users@run");
//$router->post("/users/store","Users@store");
//$router->get("/users/profile/{username}","Users@byid");
//$router->post("/users/save/comment","Comments@store");
//

$attributes = [
    'prefix' => '/users',
    'namespace' => 'App\http',
    'domain' => 'martinsblog.co.uk',
//    'middleware' => App\Middleware\AdminLogin::class,
];

// A group with many common properties!
$router->group($attributes, function (Router $router) {
    $router->get("","Users@index");
    $router->post("","Users@index");
    $router->get("/sql","Users@run");
    $router->post("/store","Users@store");
    $router->get("/profile/{username}","Users@byid");
    $router->post("/save/comment","Comments@store");
});

if(getenv("USE_AUTH") == 1)
{
  require_once(__DIR__."/App/Auth/web.php");
}
else
{
    echo "this has been disanled";
}

//Load The Routes
$router->dispatch();