<?php
//Call Autoloader and functions file
use Dotenv\Dotenv;
use Jenssegers\Blade\Blade;

session_start();
ob_start();

require_once __DIR__ . '/../vendor/autoload.php';

require_once 'Database.php';

$dotenv =  Dotenv::create(__DIR__ . "/../");
$dotenv->load();


?>