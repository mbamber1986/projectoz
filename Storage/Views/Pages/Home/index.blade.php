@extends("layouts.main")

    @section("title")
    {{getenv("APP_TITLE")}} : Homepage
    @endsection()

    @section("content")
        <p>@{{ message.split('').reverse().join('') }}</p>
        <input type="text" v-model="message" name="message"/>
    @endsection

@section("Scripts")
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!'
            }
        })
    </script>

@endsection()


