@extends("layouts.main")

@section("title")
    {{getenv("APP_TITLE")}} Profile :{{$user->username}}
@endsection
@section("content")
<h1>{{$user->username}}</h1>
<hr>

<H2>
    we have found : {{$comments->count()}} Comments for this user<br/><br>
</H2>
@foreach($comments as $comment)
    {{$comment->id}}
{!! nl2br($comment->comments) !!}<hr/>
   @endforeach

<hr>

<form action="/users/save/comment" method="post">
    <input type="hidden" name="user_id" value="{{$user->id}}">
    <textarea name="comment" id="" cols="30" rows="10"></textarea>
    <button>Add Comment</button>
</form>

    @endsection

