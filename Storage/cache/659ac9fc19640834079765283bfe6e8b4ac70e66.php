

<?php $__env->startSection("title"); ?>
    <?php echo e(getenv("APP_TITLE")); ?> Profile :<?php echo e($user->username); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("content"); ?>
<h1><?php echo e($user->username); ?></h1>
<hr>

<H2>
    we have found : <?php echo e($comments->count()); ?> Comments for this user<br/><br>
</H2>
<?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php echo e($comment->id); ?>

<?php echo nl2br($comment->comments); ?><hr/>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<hr>

<form action="/users/save/comment" method="post">
    <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
    <textarea name="comment" id="" cols="30" rows="10"></textarea>
    <button>Add Comment</button>
</form>

    <?php $__env->stopSection(); ?>


<?php echo $__env->make("layouts.main", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/projectoz/www/Storage/views/pages/users/profile.blade.php ENDPATH**/ ?>