

<?php $__env->startSection("title"); ?>
    Users : Main page
    <?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>
<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   <a href="/users/profile/<?php echo e($user->id); ?>"><?php echo e($user->username); ?></a><br>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<form action="/users" method="post">
   <input type="text" name="table">
   <textarea name="content"></textarea>
   <button name="go">Go</button>
</form>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make("layouts.admin", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/projectoz/www/Storage/views/pages/index.blade.php ENDPATH**/ ?>