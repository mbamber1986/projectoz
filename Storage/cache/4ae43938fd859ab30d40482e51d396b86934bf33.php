

    <?php $__env->startSection("title"); ?>
    <?php echo e(getenv("APP_TITLE")); ?> : Homepage
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection("content"); ?>
        <p>{{ message.split('').reverse().join('') }}</p>
        <input type="text" v-model="message" name="message"/>
    <?php $__env->stopSection(); ?>

<?php $__env->startSection("Scripts"); ?>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!'
            }
        })
    </script>

<?php $__env->stopSection(); ?>



<?php echo $__env->make("layouts.main", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/projectoz/www/Storage/views/pages/home/index.blade.php ENDPATH**/ ?>