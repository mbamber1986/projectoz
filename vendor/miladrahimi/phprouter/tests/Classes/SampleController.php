<?php

namespace MiladRahimi\PhpRouter\Tests\Classes;

/**
 * Class SampleController
 *
 * @package Classes
 * @codeCoverageIgnore
 */
class SampleController
{
    /**
     * @return string
     */
    public function home()
    {
        return 'Home';
    }
}
