<?php

namespace MiladRahimi\PhpRouter\Examples;

/**
 * Class Controller
 * Sample controller class used in examples
 *
 * @package MiladRahimi\PhpRouter\Examples
 */
class Controller
{
    /**
     * Sample controller method used in examples
     *
     * @return string
     */
    public function getAbout()
    {
        return "About me!";
    }
}
